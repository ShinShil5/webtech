<?php
    $year - $_POST["year"];

    $monthes = [
     'January' => 31,
     'February' => $year % 4 == 0 ? 29 : 28,
     'March' => 31,
     'April' => 30,
     'May' => 31,
     'June' => 30,
     'July' => 31,
     'August'=> 31,
     'September'=> 30,
     'October'=> 31,
     'November'=> 30,
     'December'=> 31
     ];

    function getCalendar() {
        return getTag('calendar','table', getMonthes())
    }    
    function getMonthes() {
        $res = '';
        foreach($monthes as $key => $month) {
            $res .= getTag('monthRow', 'tr', getMonth($key));
        }
        return $res;
    }
    function getMonth($month) {
        return getTag('month', 'td', getMonthContent($month));
    }
    function getMonthContent($month) {
        $res = '';
        $res .= getTagBegin('monthBlock', 'table');
        $res .= getTag('monthHeader', 'thead', getMonthHeader($month));
        $res .= getTag('monthBody', 'tbody', getMonthBody($monthes[$month]));
        $res .= getTagEnd('table');
        return $res;
    }
    function getMonthHeader($month) {
        return getTagBegin('', 'tr').getTagBegin('', 'td colspan="7"').$month.getTagEnd('td').getTagEnd('tr');
    }
    function getMonthBody($daysInMonth) {
        $res = '';
        for($i = 1; $i<=daysInMonth;) {
            $res .= getTagBegin('', 'tr');
            for($j = 0; $j<7 && $i<=daysInMonth; $j++, $i++) {
                $res .= getTag('monthDay', 'td', $i);
            }
            $res .= getTagEnd('tr');
        }
        return $res;
    }
    function getTagBegin($cssClass, $tag) {
        return '<'.$tag.' class="'.$cssClass.'">';
    }
    function getTagEnd($tag) {
        return '</'.$tag.'>';
    }
    function getTag($cssClass, $tag, $content) {
        return getTagBegin($cssClass, $tag).$content.getTagEnd($tag);
    }

    echo getCalendar();
?>